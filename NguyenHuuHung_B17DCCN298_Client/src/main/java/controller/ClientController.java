/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.Socket;
import model.DiemThanhPhan;
import model.Message;
import model.MonHoc;
import model.SinhVien;

/**
 *
 * @author huuhung
 */
public class ClientController {
    private String serverHost = "localhost";
    private int serverPort = 8888;

    public ClientController() {
    }
    
    public Message themSinhVien(SinhVien sinhVien) throws Exception{
        return this.query(new Message(1,sinhVien));
    }
    
    public Message suaSinhVien(SinhVien sinhVien) throws Exception{
        return this.query(new Message(2,sinhVien));
    }
    
    public Message xoaSinhVien(String maSinhVien) throws Exception{
        return this.query(new Message(3,maSinhVien));
    }
    
    public Message timKiemSinhVien(String ten) throws Exception{
        return this.query(new Message(9,ten));
    }
    
    public Message themMonHoc(MonHoc monHoc) throws Exception{
        return this.query(new Message(4,monHoc));
    }
    
    public Message suaMonHoc(MonHoc monHoc) throws Exception{
        return this.query(new Message(5,monHoc));
    }
    
    public Message xoaMonHoc(String maMonHoc) throws Exception{
        return this.query(new Message(6,maMonHoc));
    }
    
    public Message nhapDiemThanhPhan(DiemThanhPhan diemThanhPhan) throws Exception{
        return this.query(new Message(7,diemThanhPhan));
    }
    
    public Message capNhatDiemThanhPhan(DiemThanhPhan diemThanhPhan) throws Exception{
        return this.query(new Message(8,diemThanhPhan));
    }
    
    public Message timKiemMonHoc(String ten) throws Exception{
        return this.query(new Message(10,ten));
    }
    
    public Message lietKeSinhVienTheoMonHoc(String idMonHoc) throws Exception{
        return this.query(new Message(11,idMonHoc));
    }
    
    public Message layDiemThanhPhan() throws Exception{
        return this.query(new Message(12,null));
    }
    
    private Message query(Message message) throws Exception{

        Socket mySocket = new Socket(serverHost, serverPort);

        ObjectOutputStream oos = new ObjectOutputStream(mySocket.getOutputStream());

        oos.writeObject(message);
        ObjectInputStream ois = new ObjectInputStream(mySocket.getInputStream());

        message =(Message) ois.readObject();
        oos.close();
        ois.close();
        return message;
    }
}
