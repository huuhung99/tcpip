package model;

import java.io.Serializable;
import java.util.Objects;

public class MonHoc implements Serializable {
  private static final long serialVersionUID=1;
  private String ma;
  private String tenMon;
  private int soTinChi;

  public MonHoc() {
  }

  public MonHoc(String ma, String tenMon, int soTinChi) {
    this.ma = ma;
    this.tenMon = tenMon;
    this.soTinChi = soTinChi;
  }

  public String getMa() {
    return ma;
  }

  public void setMa(String ma) {
    this.ma = ma;
  }

  public String getTenMon() {
    return tenMon;
  }

  public void setTenMon(String tenMon) {
    this.tenMon = tenMon;
  }

  public int getSoTinChi() {
    return soTinChi;
  }

  public void setSoTinChi(int soTinChi) {
    this.soTinChi = soTinChi;
  }

  @Override
  public String toString() {
    return "ten mon='" + tenMon + '\'' +
        ", so tin chi=" + soTinChi ;
  }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 89 * hash + Objects.hashCode(this.ma);
        hash = 89 * hash + Objects.hashCode(this.tenMon);
        hash = 89 * hash + this.soTinChi;
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final MonHoc other = (MonHoc) obj;
        if (this.soTinChi != other.soTinChi) {
            return false;
        }
        if (!Objects.equals(this.ma, other.ma)) {
            return false;
        }
        if (!Objects.equals(this.tenMon, other.tenMon)) {
            return false;
        }
        return true;
    }
  
  
  
  public Object[] toObjects(){
        return new Object[]{ma,tenMon,soTinChi};
    }
}
