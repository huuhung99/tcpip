package model;

import java.io.Serializable;
import java.util.Date;

public class SinhVien implements Serializable {
  private static final long serialVersionUID=1;
  private String ma;
  private String hoTen;
  private String queQuan;
  private Date namSinh;

  public SinhVien() {
  }

  public SinhVien(String ma, String hoTen, String queQuan, Date namSinh) {
    this.ma = ma;
    this.hoTen = hoTen;
    this.queQuan = queQuan;
    this.namSinh = namSinh;
  }

  public String getMa() {
    return ma;
  }

  public void setMa(String ma) {
    this.ma = ma;
  }

  public String getHoTen() {
    return hoTen;
  }

  public void setHoTen(String hoTen) {
    this.hoTen = hoTen;
  }

  public String getQueQuan() {
    return queQuan;
  }

  public void setQueQuan(String queQuan) {
    this.queQuan = queQuan;
  }

  public Date getNamSinh() {
    return namSinh;
  }

  public void setNamSinh(Date namSinh) {
    this.namSinh = namSinh;
  }

  @Override
  public String toString() {
    return "hoTen='" + hoTen + '\'' +
        ", queQuan='" + queQuan + '\'' +
        ", namSinh=" + namSinh ;
  }
  public Object[] toObjects(){
        return new Object[]{ma,hoTen,queQuan,namSinh};
    }
}
