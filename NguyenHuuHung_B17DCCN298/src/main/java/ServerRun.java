import controller.Connection;
import view.ServerView;

public class ServerRun {
    public static void main(String[] args) {
        ServerView view = new ServerView();
        Connection control = new Connection(view);
    }
}
