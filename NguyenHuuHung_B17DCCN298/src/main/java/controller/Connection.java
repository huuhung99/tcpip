package controller;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.ServerSocket;
import java.net.Socket;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;
import model.DiemThanhPhan;
import model.Message;
import model.MonHoc;
import model.SinhVien;
import view.ServerView;

public class Connection {

  private final ServerView view;
  private final int serverPort = 8888;
  private java.sql.Connection con;
  private ServerSocket myServer;
  private Socket clientSocket;

  public Connection(ServerView view) {
    this.view = view;
    getDBConnection("qlsv", "root", "");
    view.showMessage("TCP server is running...");
    openServer(serverPort);
    while (true) {
      listenning();
    }
  }

  private void getDBConnection(String dbName, String username,
      String password) {

    String dbUrl = "jdbc:mysql://localhost:3306/" + dbName;
    String dbClass = "com.mysql.cj.jdbc.Driver";
    try {
      Class.forName(dbClass);
      con = DriverManager.getConnection(dbUrl,
          username, password);

    } catch (Exception e) {
    }
  }

  private void openServer(int portNumber) {
    try {
      myServer = new ServerSocket(portNumber);
    } catch (IOException e) {
    }
  }

  private void listenning() {
    try {
      clientSocket = myServer.accept();
      ObjectInputStream ois = new ObjectInputStream(clientSocket.getInputStream());
      ObjectOutputStream oos = new ObjectOutputStream(clientSocket.getOutputStream());
      Message o = (Message) ois.readObject();
      switch (o.getIndex()) {
        case 1:
          oos.writeObject(themSinhVien(o.getData()));
          break;
        case 2:
          oos.writeObject(suaSinhVien(o.getData()));
          break;
        case 3:
          oos.writeObject(xoaSinhVien(o.getData()));
          break;
        case 4:
          oos.writeObject(themMonHoc(o.getData()));
          break;
        case 5:
          oos.writeObject(suaMonHoc(o.getData()));
          break;
        case 6:
          oos.writeObject(xoaMonHoc(o.getData()));
          break;
        case 7:
          oos.writeObject(nhapDiemThanhPhan(o.getData()));
          break;
        case 8:
          oos.writeObject(capNhatDiemThanhPhan(o.getData()));
          break;
        case 9:
          oos.writeObject(timKiemSinhVien(o.getData()));
          break;
        case 10:
          oos.writeObject(timKiemMonHoc(o.getData()));
          break;
        case 11:
          oos.writeObject(lietKeSinhVienTheoMonHoc(o.getData()));
          break;
        case 12:
          oos.writeObject(layDiemThanhPhan());
          break;
        default:
          break;
      }
    } catch (Exception e) {
      e.printStackTrace();
    }
  }

  private Message layDiemThanhPhan() {
    Message message = new Message();
    List<DiemThanhPhan> diemThanhPhans = new ArrayList<>();
    String query = "SELECT d.*, s.ho_ten,s.que_quan,s.nam_sinh,m.ten_mon,m.so_tin_chi "
                 + "from diemthanhphan d "
                 + "left join sinhvien s on s.ma=d.ma_sinh_vien "
                 + "left join monhoc m on m.ma=d.ma_mon_hoc";

    try {
      PreparedStatement preparedStatement = con.prepareStatement(query);
      ResultSet rs = preparedStatement.executeQuery();
      while (rs.next()) {
        DiemThanhPhan diemThanhPhan = new DiemThanhPhan();
        MonHoc monHoc = new MonHoc();
        monHoc.setMa(rs.getString("ma_mon_hoc"));
        monHoc.setTenMon(rs.getString("ten_mon"));
        monHoc.setSoTinChi(rs.getInt("so_tin_chi"));
        diemThanhPhan.setMonHoc(monHoc);
        SinhVien sinhVien = new SinhVien();
        sinhVien.setMa(rs.getString("ma_sinh_vien"));
        sinhVien.setHoTen(rs.getString("ho_ten"));
        sinhVien.setQueQuan(rs.getString("que_quan"));
        sinhVien.setNamSinh(rs.getDate("nam_sinh"));
        diemThanhPhan.setMa(rs.getString("ma"));
        diemThanhPhan.setSinhVien(sinhVien);
        diemThanhPhan.setChuyenCan(rs.getFloat("chuyen_can"));
        diemThanhPhan.setBaiTapLon(rs.getFloat("bai_tap_lon"));
        diemThanhPhan.setDiemThi(rs.getFloat("diem_thi"));
        diemThanhPhans.add(diemThanhPhan);
      }
      message.setData(diemThanhPhans);
      preparedStatement.close();
    } catch (Exception e) {
      e.printStackTrace();
      message.setIndex(400);
      message.setData("That bai!");
      return message;
    }
    System.out.println(message);
    return message;
  }

  private Message timKiemMonHoc(Object data) {
    Message message = new Message();
    List<MonHoc> monHocs = new ArrayList<>();
    String ten = (String) data;
    String query = "SELECT * from monhoc "
        + "where (? is null or monhoc.ten_mon like ?) and trang_thai= 'active'";

    try {
      PreparedStatement preparedStatement = con.prepareStatement(query);
      preparedStatement.setString(1, ten);
      preparedStatement.setString(2, "%" + ten + "%");
      ResultSet rs = preparedStatement.executeQuery();
      while (rs.next()) {
        MonHoc monHoc = new MonHoc();
        monHoc.setMa(rs.getString("ma"));
        monHoc.setTenMon(rs.getString("ten_mon"));
        monHoc.setSoTinChi(rs.getInt("so_tin_chi"));
        monHocs.add(monHoc);
      }
      message.setData(monHocs);
      preparedStatement.close();
    } catch (Exception e) {
      e.printStackTrace();
      message.setIndex(400);
      message.setData("That bai!");
      return message;
    }
    System.out.println(message);
    return message;
  }

  private Message lietKeSinhVienTheoMonHoc(Object data) {
    Message message = new Message();
    List<SinhVien> sinhViens = new ArrayList<>();
    String idMonHoc = (String) data;
    String query = "SELECT s.* from diemthanhphan d INNER JOIN sinhvien s ON s.ma=d.ma_sinh_vien WHERE d.ma_mon_hoc=?";

    try {
      PreparedStatement preparedStatement = con.prepareStatement(query);
      preparedStatement.setString(1, idMonHoc);
      ResultSet rs = preparedStatement.executeQuery();
      while (rs.next()) {
        SinhVien sinhVien = new SinhVien();
        sinhVien.setMa(rs.getString("ma"));
        sinhVien.setHoTen(rs.getString("ho_ten"));
        sinhVien.setQueQuan(rs.getString("que_quan"));
        sinhVien.setNamSinh(rs.getDate("nam_sinh"));
        sinhViens.add(sinhVien);
      }
      message.setData(sinhViens);
      preparedStatement.close();
    } catch (Exception e) {
      e.printStackTrace();
      message.setIndex(400);
      message.setData("That bai!");
      return message;
    }
    System.out.println(message);
    return message;
  }

  private Message timKiemSinhVien(Object data) {
    Message message = new Message();
    List<SinhVien> sinhViens = new ArrayList<>();
    String ten = (String) data;
    String query = "SELECT * from sinhvien where (? is null or sinhvien.ho_ten like ?"
        + " or sinhvien.que_quan like ? or MONTH(sinhvien.nam_sinh) = ? "
        + "or YEAR(sinhvien.nam_sinh) = ? ) and trang_thai ='active'";

    try {
      PreparedStatement preparedStatement = con.prepareStatement(query);
      preparedStatement.setString(1, ten);
      preparedStatement.setString(2, "%" + ten + "%");
      preparedStatement.setString(3, "%" + ten + "%");
      preparedStatement.setString(4, ten);
      preparedStatement.setString(5, ten);
      ResultSet rs = preparedStatement.executeQuery();
      while (rs.next()) {
        SinhVien sinhVien = new SinhVien();
        sinhVien.setMa(rs.getString("ma"));
        sinhVien.setHoTen(rs.getString("ho_ten"));
        sinhVien.setQueQuan(rs.getString("que_quan"));
        sinhVien.setNamSinh(rs.getDate("nam_sinh"));
        sinhViens.add(sinhVien);
      }
      message.setData(sinhViens);
      preparedStatement.close();
    } catch (Exception e) {
      e.printStackTrace();
      message.setIndex(400);
      message.setData("That bai!");
      return message;
    }
    System.out.println(message);
    return message;
  }

  private Message capNhatDiemThanhPhan(Object data) {
    Message message = new Message();
    DiemThanhPhan diemThanhPhan = (DiemThanhPhan) data;
    String query = "UPDATE diemthanhphan"
        + " SET chuyen_can=?,bai_tap_lon=?,diem_thi=? "
        + "WHERE ma=?";
    try {
      PreparedStatement preparedStatement = con.prepareStatement(query);
      preparedStatement.setFloat(1, diemThanhPhan.getChuyenCan());
      preparedStatement.setFloat(2, diemThanhPhan.getBaiTapLon());
      preparedStatement.setFloat(3, diemThanhPhan.getDiemThi());
      preparedStatement.setString(4, diemThanhPhan.getMa());
      int rs = preparedStatement.executeUpdate();
      if (rs > 0) {
        message.setIndex(200);
        message.setData("Thanh cong!");
      }
      preparedStatement.close();
    } catch (Exception e) {
      e.printStackTrace();
      message.setIndex(400);
      message.setData("That bai!");
      return message;
    }
    System.out.println(message);
    return message;
  }

  private Message nhapDiemThanhPhan(Object data) {
    Message message = new Message();
    DiemThanhPhan diemThanhPhan = (DiemThanhPhan) data;
    if(checkDiemThanhPhan(diemThanhPhan.getSinhVien(),diemThanhPhan.getMonHoc())){
      message.setIndex(400);
      message.setData("Sinh viên đã có điểm thành phần môn học này!");
      return message;
    }
    String query = "INSERT INTO diemthanhphan(ma,ma_sinh_vien,ma_mon_hoc"
        + ",chuyen_can,bai_tap_lon,diem_thi) values (?,?,?,?,?,?)";
    try {
      PreparedStatement preparedStatement = con.prepareStatement(query);
      preparedStatement.setString(1, UUID.randomUUID().toString());
      preparedStatement.setString(2, diemThanhPhan.getSinhVien().getMa());
      preparedStatement.setString(3, diemThanhPhan.getMonHoc().getMa());
      preparedStatement.setFloat(4, diemThanhPhan.getChuyenCan());
      preparedStatement.setFloat(5, diemThanhPhan.getBaiTapLon());
      preparedStatement.setFloat(6, diemThanhPhan.getDiemThi());
      int rs = preparedStatement.executeUpdate();
      if (rs > 0) {
        message.setIndex(200);
        message.setData("Thanh cong!");
      }
      preparedStatement.close();
    } catch (Exception e) {
      message.setIndex(400);
      message.setData("That bai!");
      e.printStackTrace();
      return message;
    }
    System.out.println(message);
    return message;
  }

  private boolean checkDiemThanhPhan(SinhVien sinhVien, MonHoc monHoc) {
    String query= "SELECT d.ma FROM diemthanhphan d WHERE d.ma_sinh_vien =? AND d.ma_mon_hoc = ?";
    try {
      PreparedStatement preparedStatement = con.prepareStatement(query);
      preparedStatement.setString(1, sinhVien.getMa());
      preparedStatement.setString(2, monHoc.getMa());
      ResultSet rs = preparedStatement.executeQuery();
      while (rs.next()) {
        String string = rs.getString(1);
        System.out.println(string);
        return true;
      }
      preparedStatement.close();
    } catch (Exception e) {
      e.printStackTrace();
    }
    return false;
  }

  private Message xoaMonHoc(Object data) {
    Message message = new Message();
    String ma = (String) data;
    String query = "UPDATE monhoc SET trang_thai = 'delete' WHERE ma=?";
    try {
      PreparedStatement preparedStatement = con.prepareStatement(query);
      preparedStatement.setString(1, ma);
      int rs = preparedStatement.executeUpdate();
      if (rs > 0) {
        message.setIndex(200);
        message.setData("Thanh cong!");
      }
      preparedStatement.close();
    } catch (Exception e) {
      message.setIndex(400);
      message.setData("That bai!");
      e.printStackTrace();
      return message;
    }
    System.out.println(message);
    return message;
  }

  private Message suaMonHoc(Object data) {
    Message message = new Message();
    MonHoc monHoc = (MonHoc) data;
    String query = "UPDATE monhoc "
        + "SET ten_mon=?,so_tin_chi=? "
        + "WHERE ma=?";
    try {
      PreparedStatement preparedStatement = con.prepareStatement(query);
      preparedStatement.setString(1, monHoc.getTenMon());
      preparedStatement.setInt(2, monHoc.getSoTinChi());
      preparedStatement.setString(3, monHoc.getMa());
      int rs = preparedStatement.executeUpdate();
      if (rs > 0) {
        message.setIndex(200);
        message.setData("Thanh cong!");
      }
      preparedStatement.close();
    } catch (Exception e) {
      message.setIndex(400);
      message.setData("That bai!");
      e.printStackTrace();
      return message;
    }
    System.out.println(message);
    return message;
  }

  private Message themMonHoc(Object data) {
    Message message = new Message();
    MonHoc monHoc = (MonHoc) data;
    String query = "INSERT INTO monhoc(ma,ten_mon,so_tin_chi,trang_thai) values (?,?,?,?)";
    try {
      PreparedStatement preparedStatement = con.prepareStatement(query);
      preparedStatement.setString(1, UUID.randomUUID().toString());
      preparedStatement.setString(2, monHoc.getTenMon());
      preparedStatement.setInt(3, monHoc.getSoTinChi());
      preparedStatement.setString(4, "active");
      int rs = preparedStatement.executeUpdate();
      if (rs > 0) {
        message.setIndex(200);
        message.setData("Thanh cong!");
      }
      preparedStatement.close();
    } catch (Exception e) {
      message.setIndex(400);
      message.setData("That bai!");
      e.printStackTrace();
      return message;
    }
    System.out.println(message);
    return message;
  }

  private Message xoaSinhVien(Object data) {
    Message message = new Message();
    String ma = (String) data;
    String query = "UPDATE sinhvien SET trang_thai = 'delete' WHERE ma=?";
    try {
      PreparedStatement preparedStatement = con.prepareStatement(query);
      preparedStatement.setString(1, ma);
      int rs = preparedStatement.executeUpdate();
      if (rs > 0) {
        message.setIndex(200);
        message.setData("Thanh cong!");
      }
      preparedStatement.close();
    } catch (Exception e) {
      message.setIndex(400);
      message.setData("That bai!");
      e.printStackTrace();
      return message;
    }
    System.out.println(message);
    return message;
  }

  private Message suaSinhVien(Object data) {
    Message message = new Message();
    SinhVien sinhVien = (SinhVien) data;
    String query = "UPDATE sinhvien "
        + "SET ho_ten=?,que_quan=?,nam_sinh=? "
        + "WHERE ma=?";
    try {
      PreparedStatement preparedStatement = con.prepareStatement(query);
      preparedStatement.setString(1, sinhVien.getHoTen());
      preparedStatement.setString(2, sinhVien.getQueQuan());
      preparedStatement.setDate(3, new java.sql.Date(sinhVien.getNamSinh().getTime()));
      preparedStatement.setString(4, sinhVien.getMa());
      int rs = preparedStatement.executeUpdate();
      if (rs > 0) {
        message.setIndex(200);
        message.setData("Thanh cong!");
      }
      preparedStatement.close();
    } catch (Exception e) {
      message.setIndex(400);
      message.setData("That bai!");
      e.printStackTrace();
      return message;
    }
    System.out.println(message);
    return message;
  }

  private Message themSinhVien(Object data) {
    Message message = new Message();
    SinhVien sinhVien = (SinhVien) data;
    String query = "INSERT INTO sinhvien(ma,ho_ten,que_quan,nam_sinh,trang_thai) values (?,?,?,?,?)";
    try {
      PreparedStatement preparedStatement = con.prepareStatement(query);
      preparedStatement.setString(1, UUID.randomUUID().toString());
      preparedStatement.setString(2, sinhVien.getHoTen());
      preparedStatement.setString(3, sinhVien.getQueQuan());
      preparedStatement.setDate(4, new java.sql.Date(sinhVien.getNamSinh().getTime()));
      preparedStatement.setString(5, "active");
      int rs = preparedStatement.executeUpdate();
      if (rs > 0) {
        message.setIndex(200);
        message.setData("Thanh cong!");
      }
      preparedStatement.close();
    } catch (Exception e) {
      message.setIndex(400);
      message.setData("That bai!");
      e.printStackTrace();
      return message;
    }
    System.out.println(message);
    return message;
  }
}
