package model;

import java.io.Serializable;

public class MonHoc implements Serializable {
  private static final long serialVersionUID=1;
  private String ma;
  private String tenMon;
  private int soTinChi;

  public MonHoc() {
  }

  public MonHoc(String ma, String tenMon, int soTinChi) {
    this.ma = ma;
    this.tenMon = tenMon;
    this.soTinChi = soTinChi;
  }

  public String getMa() {
    return ma;
  }

  public void setMa(String ma) {
    this.ma = ma;
  }

  public String getTenMon() {
    return tenMon;
  }

  public void setTenMon(String tenMon) {
    this.tenMon = tenMon;
  }

  public int getSoTinChi() {
    return soTinChi;
  }

  public void setSoTinChi(int soTinChi) {
    this.soTinChi = soTinChi;
  }

  @Override
  public String toString() {
    return "MonHoc{" +
        "ma=" + ma +
        ", tenMon='" + tenMon + '\'' +
        ", soTinChi=" + soTinChi +
        '}';
  }
}
