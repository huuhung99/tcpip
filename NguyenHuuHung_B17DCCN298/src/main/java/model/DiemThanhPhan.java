package model;

import java.io.Serializable;

public class DiemThanhPhan implements Serializable {
  private static final long serialVersionUID=1;
  private String ma;
  private MonHoc monHoc;
  private SinhVien sinhVien;
  private Float chuyenCan;
  private Float baiTapLon;
  private Float diemThi;

  public DiemThanhPhan(String ma, MonHoc monHoc, SinhVien sinhVien, Float chuyenCan,
      Float baiTapLon, Float diemThi) {
    this.ma = ma;
    this.monHoc = monHoc;
    this.sinhVien = sinhVien;
    this.chuyenCan = chuyenCan;
    this.baiTapLon = baiTapLon;
    this.diemThi = diemThi;
  }

  public DiemThanhPhan() {
  }

  public String getMa() {
    return ma;
  }

  public void setMa(String ma) {
    this.ma = ma;
  }

  public MonHoc getMonHoc() {
    return monHoc;
  }

  public void setMonHoc(MonHoc monHoc) {
    this.monHoc = monHoc;
  }

  public SinhVien getSinhVien() {
    return sinhVien;
  }

  public void setSinhVien(SinhVien sinhVien) {
    this.sinhVien = sinhVien;
  }

  public Float getChuyenCan() {
    return chuyenCan;
  }

  public void setChuyenCan(Float chuyenCan) {
    this.chuyenCan = chuyenCan;
  }

  public Float getBaiTapLon() {
    return baiTapLon;
  }

  public void setBaiTapLon(Float baiTapLon) {
    this.baiTapLon = baiTapLon;
  }

  public Float getDiemThi() {
    return diemThi;
  }

  public void setDiemThi(Float diemThi) {
    this.diemThi = diemThi;
  }

  @Override
  public String toString() {
    return "DiemThanhPhan{" +
        "ma='" + ma + '\'' +
        ", monHoc=" + monHoc +
        ", sinhVien=" + sinhVien +
        ", chuyenCan=" + chuyenCan +
        ", baiTapLon=" + baiTapLon +
        ", diemThi=" + diemThi +
        '}';
  }
}
