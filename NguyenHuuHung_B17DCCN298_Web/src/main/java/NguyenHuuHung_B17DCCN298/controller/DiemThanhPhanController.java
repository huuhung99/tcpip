package NguyenHuuHung_B17DCCN298.controller;

import NguyenHuuHung_B17DCCN298.model.DiemThanhPhan;
import NguyenHuuHung_B17DCCN298.repository.DiemThanhPhanRepository;
import NguyenHuuHung_B17DCCN298.repository.MonHocRepository;
import java.util.List;
import java.util.stream.Collectors;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

@Controller
@RequestMapping("/diemthanhphan")
public class DiemThanhPhanController {

  @Autowired
  DiemThanhPhanRepository diemThanhPhanRepository;

  @Autowired
  MonHocRepository monHocRepository;

  @GetMapping
  public String home(Model model) {
    model.addAttribute("diemThanhPhans", diemThanhPhanRepository.findAll());
    return "DiemThanhPhan";
  }

  @GetMapping("/{ma}")
  public String getSinhVienTheoMonHoc(Model model, @PathVariable String ma) {
    List<DiemThanhPhan> allByMonHoc = diemThanhPhanRepository.getAllByMaMonHoc(ma);
    model.addAttribute("diemThanhPhans", allByMonHoc);
    return "DiemThanhPhan";
  }

  @GetMapping("/dieu-kien/{ma}")
  public String getSinhVienTheoMonHocDieuKien(Model model, @PathVariable String ma) {
    List<DiemThanhPhan> allByMonHoc = diemThanhPhanRepository.getAllByMaMonHoc(ma).stream()
        .filter(d -> d.getBaiTapLon() >= 8.0).collect(
            Collectors.toList());
    model.addAttribute("diemThanhPhans", allByMonHoc);
    return "DiemThanhPhan";
  }
  @GetMapping("/tim")
  private String timDienThoai(Model model, @RequestParam("keyword") String keyword) {
    if(keyword!=null&& !keyword.isEmpty()){
      keyword = "%"+ keyword + "%";
      model.addAttribute("diemThanhPhans", diemThanhPhanRepository.search(keyword));
      return "DiemThanhPhan";
    }
    return "redirect:/DiemThanhPhan";
  }
}
