package NguyenHuuHung_B17DCCN298.controller;

import NguyenHuuHung_B17DCCN298.repository.MonHocRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

@Controller
@RequestMapping("/monhoc")
public class MonHocController {

  @Autowired
  MonHocRepository monHocRepository;

  @GetMapping
  public String home(Model model) {
    model.addAttribute("monHocs", monHocRepository.findAll());
    return "MonHoc";
  }

  @GetMapping("/tim")
  private String timDienThoai(Model model, @RequestParam("keyword") String keyword) {
    if (keyword != null && !keyword.isEmpty()) {
      model.addAttribute("monHocs",
          monHocRepository.getAllByTenMonContains(keyword));
      return "MonHoc";
    }
    return "redirect:/monhoc";
  }
}
