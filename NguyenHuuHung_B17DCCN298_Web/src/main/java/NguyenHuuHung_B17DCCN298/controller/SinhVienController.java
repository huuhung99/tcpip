package NguyenHuuHung_B17DCCN298.controller;

import NguyenHuuHung_B17DCCN298.repository.SinhVienRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

@Controller
@RequestMapping("/")
public class SinhVienController {

  @Autowired
  SinhVienRepository sinhVienRepository;

  @GetMapping
  public String home(Model model) {
    model.addAttribute("sinhViens", sinhVienRepository.findAll());
    return "SinhVien";
  }

  @GetMapping("/tim")
  private String timDienThoai(Model model, @RequestParam("keyword") String keyword) {
    if (keyword != null && !keyword.isEmpty()) {
      model.addAttribute("sinhViens",
          sinhVienRepository.getAllByHoTenContainsOrQueQuanContains(keyword,keyword));
      return "SinhVien";
    }
    return "redirect:/";
  }
}
