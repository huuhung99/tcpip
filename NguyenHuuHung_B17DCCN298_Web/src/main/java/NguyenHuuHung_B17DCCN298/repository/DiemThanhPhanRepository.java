package NguyenHuuHung_B17DCCN298.repository;

import NguyenHuuHung_B17DCCN298.model.DiemThanhPhan;
import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

public interface DiemThanhPhanRepository extends JpaRepository<DiemThanhPhan,String> {
  @Query("SELECT d FROM DiemThanhPhan d WHERE d.monHoc.ma = :ma ")
  List<DiemThanhPhan> getAllByMaMonHoc(String ma);

  @Query("SELECT d FROM DiemThanhPhan d WHERE d.monHoc.tenMon like :key or d.sinhVien.hoTen like :key")
  List<DiemThanhPhan> search(String key);
}
