package NguyenHuuHung_B17DCCN298.repository;

import NguyenHuuHung_B17DCCN298.model.SinhVien;
import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;

public interface SinhVienRepository extends JpaRepository<SinhVien,String> {
  List<SinhVien> getAllByHoTenContainsOrQueQuanContains(String hoTen,String queQuan);
}
