package NguyenHuuHung_B17DCCN298.repository;

import NguyenHuuHung_B17DCCN298.model.MonHoc;
import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;

public interface MonHocRepository extends JpaRepository<MonHoc,String> {
  List<MonHoc> getAllByTenMonContains(String key);
}
