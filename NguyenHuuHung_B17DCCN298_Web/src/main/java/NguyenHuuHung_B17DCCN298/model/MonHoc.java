package NguyenHuuHung_B17DCCN298.model;

import java.io.Serializable;
import java.util.List;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Table(name = "monhoc")
@Entity
public class MonHoc implements Serializable {
  @Id
  private String ma;
  @Column(name = "ten_mon")
  private String tenMon;
  @Column(name = "so_tin_chi")
  private int soTinChi;
  @Column(name = "trang_thai")
  private String trangThai;

  @OneToMany(mappedBy = "monHoc")
  private List<DiemThanhPhan> diemThanhPhans;

  public MonHoc() {
  }

  public String getTrangThai() {
    return trangThai;
  }

  public void setTrangThai(String trangThai) {
    this.trangThai = trangThai;
  }

  public MonHoc(String ma, String tenMon, int soTinChi) {
    this.ma = ma;
    this.tenMon = tenMon;
    this.soTinChi = soTinChi;
  }

  public String getMa() {
    return ma;
  }

  public void setMa(String ma) {
    this.ma = ma;
  }

  public String getTenMon() {
    return tenMon;
  }

  public void setTenMon(String tenMon) {
    this.tenMon = tenMon;
  }

  public int getSoTinChi() {
    return soTinChi;
  }

  public void setSoTinChi(int soTinChi) {
    this.soTinChi = soTinChi;
  }

  @Override
  public String toString() {
    return "MonHoc{" +
        "ma=" + ma +
        ", tenMon='" + tenMon + '\'' +
        ", soTinChi=" + soTinChi +
        '}';
  }
}
