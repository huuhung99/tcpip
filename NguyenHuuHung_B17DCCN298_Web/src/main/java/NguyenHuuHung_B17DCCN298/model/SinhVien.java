package NguyenHuuHung_B17DCCN298.model;

import java.io.Serializable;
import java.util.Date;
import java.util.List;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name = "sinhvien")
public class SinhVien implements Serializable {
  @Id
  private String ma;
  @Column(name = "ho_ten")
  private String hoTen;
  @Column(name = "que_quan")
  private String queQuan;
  @Column(name = "trang_thai")
  private String trangThai;
  @Column(name = "nam_sinh")
  private Date namSinh;

  @OneToMany(mappedBy = "monHoc")
  private List<DiemThanhPhan> diemThanhPhans;
  public SinhVien() {
  }

  public String getTrangThai() {
    return trangThai;
  }

  public void setTrangThai(String trangThai) {
    this.trangThai = trangThai;
  }

  public SinhVien(String ma, String hoTen, String queQuan, Date namSinh) {
    this.ma = ma;
    this.hoTen = hoTen;
    this.queQuan = queQuan;
    this.namSinh = namSinh;
  }

  public String getMa() {
    return ma;
  }

  public void setMa(String ma) {
    this.ma = ma;
  }

  public String getHoTen() {
    return hoTen;
  }

  public void setHoTen(String hoTen) {
    this.hoTen = hoTen;
  }

  public String getQueQuan() {
    return queQuan;
  }

  public void setQueQuan(String queQuan) {
    this.queQuan = queQuan;
  }

  public Date getNamSinh() {
    return namSinh;
  }

  public void setNamSinh(Date namSinh) {
    this.namSinh = namSinh;
  }

  @Override
  public String toString() {
    return "SinhVien{" +
        "ma=" + ma +
        ", hoTen='" + hoTen + '\'' +
        ", queQuan='" + queQuan + '\'' +
        ", namSinh=" + namSinh +
        '}';
  }
}
