package NguyenHuuHung_B17DCCN298.model;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Table(name = "diemthanhphan")
@Entity
public class DiemThanhPhan implements Serializable {
  @Id
  private String ma;
  @ManyToOne
  @JoinColumn(name = "ma_mon_hoc")
  private MonHoc monHoc;
  @ManyToOne
  @JoinColumn(name = "ma_sinh_vien")
  private SinhVien sinhVien;
  @Column(name = "chuyen_can")
  private Float chuyenCan;
  @Column(name = "bai_tap_lon")
  private Float baiTapLon;
  @Column(name = "diem_thi")
  private Float diemThi;

  public DiemThanhPhan(String ma, MonHoc monHoc, SinhVien sinhVien, Float chuyenCan,
      Float baiTapLon, Float diemThi) {
    this.ma = ma;
    this.monHoc = monHoc;
    this.sinhVien = sinhVien;
    this.chuyenCan = chuyenCan;
    this.baiTapLon = baiTapLon;
    this.diemThi = diemThi;
  }

  public DiemThanhPhan() {
  }

  public String getMa() {
    return ma;
  }

  public void setMa(String ma) {
    this.ma = ma;
  }

  public MonHoc getMonHoc() {
    return monHoc;
  }

  public void setMonHoc(MonHoc monHoc) {
    this.monHoc = monHoc;
  }

  public SinhVien getSinhVien() {
    return sinhVien;
  }

  public void setSinhVien(SinhVien sinhVien) {
    this.sinhVien = sinhVien;
  }

  public Float getChuyenCan() {
    return chuyenCan;
  }

  public void setChuyenCan(Float chuyenCan) {
    this.chuyenCan = chuyenCan;
  }

  public Float getBaiTapLon() {
    return baiTapLon;
  }

  public void setBaiTapLon(Float baiTapLon) {
    this.baiTapLon = baiTapLon;
  }

  public Float getDiemThi() {
    return diemThi;
  }

  public void setDiemThi(Float diemThi) {
    this.diemThi = diemThi;
  }
}
