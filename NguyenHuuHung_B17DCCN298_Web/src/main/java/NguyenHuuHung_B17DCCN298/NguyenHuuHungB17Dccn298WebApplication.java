package NguyenHuuHung_B17DCCN298;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class NguyenHuuHungB17Dccn298WebApplication {

  public static void main(String[] args) {
    SpringApplication.run(NguyenHuuHungB17Dccn298WebApplication.class, args);
  }

}
